/*
 * Automatically generated by make menuconfig: don't edit
 */
#define AUTOCONF_INCLUDED

/*
 * Vendor/Product Selection
 */
#undef  CONFIG_DEFAULTS_3COM
#undef  CONFIG_DEFAULTS_ACTIONTEC
#undef  CONFIG_DEFAULTS_ADI
#undef  CONFIG_DEFAULTS_AKIZUKI
#undef  CONFIG_DEFAULTS_ALTERA
#undef  CONFIG_DEFAULTS_APPLE
#undef  CONFIG_DEFAULTS_ARCTURUS
#undef  CONFIG_DEFAULTS_ARNEWSH
#undef  CONFIG_DEFAULTS_ATMARKTECHNO
#undef  CONFIG_DEFAULTS_ATMEL
#undef  CONFIG_DEFAULTS_AVNET
#undef  CONFIG_DEFAULTS_CIRRUS
#undef  CONFIG_DEFAULTS_COGENT
#undef  CONFIG_DEFAULTS_CONEXANT
#undef  CONFIG_DEFAULTS_CWLINUX
#undef  CONFIG_DEFAULTS_CYBERGUARD
#undef  CONFIG_DEFAULTS_CYTEK
#undef  CONFIG_DEFAULTS_EMAC
#undef  CONFIG_DEFAULTS_ESPD
#undef  CONFIG_DEFAULTS_EXYS
#undef  CONFIG_DEFAULTS_FEITH
#undef  CONFIG_DEFAULTS_FREESCALE
#undef  CONFIG_DEFAULTS_FUTURE
#undef  CONFIG_DEFAULTS_GDB
#undef  CONFIG_DEFAULTS_HITACHI
#undef  CONFIG_DEFAULTS_IMT
#undef  CONFIG_DEFAULTS_INSIGHT
#undef  CONFIG_DEFAULTS_INTEL
#undef  CONFIG_DEFAULTS_KENDINMICREL
#undef  CONFIG_DEFAULTS_LEOX
#undef  CONFIG_DEFAULTS_MECEL
#undef  CONFIG_DEFAULTS_MIDAS
#undef  CONFIG_DEFAULTS_NEC
#undef  CONFIG_DEFAULTS_NETBURNER
#undef  CONFIG_DEFAULTS_NETSILICON
#undef  CONFIG_DEFAULTS_NINTENDO
#define CONFIG_DEFAULTS_NXP 1
#undef  CONFIG_DEFAULTS_OPENCORES
#undef  CONFIG_DEFAULTS_OPENGEAR
#undef  CONFIG_DEFAULTS_PHILIPS
#undef  CONFIG_DEFAULTS_PROMISE
#undef  CONFIG_DEFAULTS_SAMSUNG
#undef  CONFIG_DEFAULTS_SECURECOMPUTING
#undef  CONFIG_DEFAULTS_SECUREEDGE
#undef  CONFIG_DEFAULTS_SENTEC
#undef  CONFIG_DEFAULTS_SIGNAL
#undef  CONFIG_DEFAULTS_SIMTEC
#undef  CONFIG_DEFAULTS_SNAPGEAR
#undef  CONFIG_DEFAULTS_SNEHA
#undef  CONFIG_DEFAULTS_SOEKRIS
#undef  CONFIG_DEFAULTS_SONY
#undef  CONFIG_DEFAULTS_SSV
#undef  CONFIG_DEFAULTS_STRAWBERRYLINUX
#undef  CONFIG_DEFAULTS_SWARM
#undef  CONFIG_DEFAULTS_TELEIP
#undef  CONFIG_DEFAULTS_TI
#undef  CONFIG_DEFAULTS_TRISCEND
#undef  CONFIG_DEFAULTS_VIA
#undef  CONFIG_DEFAULTS_WEISS
#undef  CONFIG_DEFAULTS_XILINX
#define CONFIG_DEFAULTS_NXP_LPC2468 1

/*
 * Kernel/Library/Defaults Selection
 */
#undef  CONFIG_DEFAULTS_KERNEL_2_6_24_2_lpc2478_patched
#define CONFIG_DEFAULTS_KERNEL_2_6 1
#undef  CONFIG_DEFAULTS_LIBC_NONE
#undef  CONFIG_DEFAULTS_LIBC_UC_LIBC
#define CONFIG_DEFAULTS_LIBC_UCLIBC 1
#undef  CONFIG_DEFAULTS_OVERRIDE
#undef  CONFIG_DEFAULTS_KERNEL
#undef  CONFIG_DEFAULTS_VENDOR
#undef  CONFIG_DEFAULTS_VENDOR_UPDATE
