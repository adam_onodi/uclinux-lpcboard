/*
 * Automatically generated header file: don't edit
 */

#define AUTOCONF_INCLUDED

/* Version Number */
#define BB_VER "1.00"
#define BB_BT "2008.10.07-02:27+0000"

#define HAVE_DOT_CONFIG 1

/*
 * General Configuration
 */
#define CONFIG_FEATURE_BUFFERS_USE_MALLOC 1
#undef CONFIG_FEATURE_BUFFERS_GO_ON_STACK
#undef CONFIG_FEATURE_BUFFERS_GO_IN_BSS
#undef CONFIG_FEATURE_VERBOSE_USAGE
#undef CONFIG_FEATURE_INSTALLER
#undef CONFIG_LOCALE_SUPPORT
#undef CONFIG_FEATURE_DEVFS
#undef CONFIG_FEATURE_DEVPTS
#undef CONFIG_FEATURE_CLEAN_UP
#define CONFIG_FEATURE_SUID 1
#undef CONFIG_FEATURE_SUID_CONFIG
#undef CONFIG_SELINUX

/*
 * Build Options
 */
#define CONFIG_STATIC 1
#undef CONFIG_LFS
#undef USING_CROSS_COMPILER
#define EXTRA_CFLAGS_OPTIONS ""

/*
 * Installation Options
 */
#undef CONFIG_INSTALL_NO_USR
#define PREFIX "./_install"

/*
 * Archival Utilities
 */
#define CONFIG_AR 1
#undef CONFIG_FEATURE_AR_LONG_FILENAMES
#define CONFIG_BUNZIP2 1
#define CONFIG_CPIO 1
#undef CONFIG_DPKG
#undef CONFIG_DPKG_DEB
#define CONFIG_GUNZIP 1
#define CONFIG_FEATURE_GUNZIP_UNCOMPRESS 1
#define CONFIG_GZIP 1
#undef CONFIG_RPM2CPIO
#undef CONFIG_RPM
#define CONFIG_TAR 1
#define CONFIG_FEATURE_TAR_CREATE 1
#define CONFIG_FEATURE_TAR_BZIP2 1
#define CONFIG_FEATURE_TAR_FROM 1
#define CONFIG_FEATURE_TAR_GZIP 1
#define CONFIG_FEATURE_TAR_COMPRESS 1
#define CONFIG_FEATURE_TAR_OLDGNU_COMPATABILITY 1
#define CONFIG_FEATURE_TAR_GNU_EXTENSIONS 1
#define CONFIG_FEATURE_TAR_LONG_OPTIONS 1
#undef CONFIG_UNCOMPRESS
#define CONFIG_UNZIP 1

/*
 * Common options for cpio and tar
 */
#undef CONFIG_FEATURE_UNARCHIVE_TAPE

/*
 * Coreutils
 */
#define CONFIG_BASENAME 1
#define CONFIG_CAL 1
#define CONFIG_CAT 1
#define CONFIG_CHGRP 1
#define CONFIG_CHMOD 1
#define CONFIG_CHOWN 1
#undef CONFIG_CHROOT
#define CONFIG_CMP 1
#define CONFIG_CP 1
#define CONFIG_CUT 1
#define CONFIG_DATE 1
#define CONFIG_FEATURE_DATE_ISOFMT 1
#define CONFIG_DD 1
#define CONFIG_DF 1
#define CONFIG_DIRNAME 1
#define CONFIG_DOS2UNIX 1
#define CONFIG_UNIX2DOS 1
#define CONFIG_DU 1
#define CONFIG_FEATURE_DU_DEFALT_BLOCKSIZE_1K 1
#define CONFIG_ECHO 1
#define CONFIG_FEATURE_FANCY_ECHO 1
#define CONFIG_ENV 1
#define CONFIG_EXPR 1
#define CONFIG_FALSE 1

/*
 * false (forced enabled for use with shell)
 */
#undef CONFIG_FOLD
#define CONFIG_HEAD 1
#define CONFIG_FEATURE_FANCY_HEAD 1
#define CONFIG_HOSTID 1
#undef CONFIG_ID
#undef CONFIG_INSTALL
#undef CONFIG_LENGTH
#define CONFIG_LN 1
#undef CONFIG_LOGNAME
#define CONFIG_LS 1
#define CONFIG_FEATURE_LS_FILETYPES 1
#define CONFIG_FEATURE_LS_FOLLOWLINKS 1
#define CONFIG_FEATURE_LS_RECURSIVE 1
#define CONFIG_FEATURE_LS_SORTFILES 1
#define CONFIG_FEATURE_LS_TIMESTAMPS 1
#define CONFIG_FEATURE_LS_USERNAME 1
#define CONFIG_FEATURE_LS_COLOR 1
#define CONFIG_MD5SUM 1
#define CONFIG_MKDIR 1
#define CONFIG_MKFIFO 1
#define CONFIG_MKNOD 1
#define CONFIG_MV 1
#define CONFIG_OD 1
#define CONFIG_PRINTF 1
#define CONFIG_PWD 1
#undef CONFIG_REALPATH
#define CONFIG_RM 1
#define CONFIG_RMDIR 1
#undef CONFIG_SEQ
#define CONFIG_SHA1SUM 1
#define CONFIG_SLEEP 1
#undef CONFIG_FEATURE_FANCY_SLEEP
#define CONFIG_SORT 1
#undef CONFIG_STTY
#define CONFIG_SYNC 1
#define CONFIG_TAIL 1
#define CONFIG_FEATURE_FANCY_TAIL 1
#define CONFIG_TEE 1
#undef CONFIG_FEATURE_TEE_USE_BLOCK_IO
#define CONFIG_TEST 1

/*
 * test (forced enabled for use with shell)
 */
#undef CONFIG_FEATURE_TEST_64
#define CONFIG_TOUCH 1
#define CONFIG_TR 1
#define CONFIG_TRUE 1

/*
 * true (forced enabled for use with shell)
 */
#define CONFIG_TTY 1
#define CONFIG_UNAME 1
#undef CONFIG_UNIQ
#define CONFIG_USLEEP 1
#undef CONFIG_UUDECODE
#undef CONFIG_UUENCODE
#undef CONFIG_WATCH
#define CONFIG_WC 1
#undef CONFIG_WHO
#undef CONFIG_WHOAMI
#undef CONFIG_YES

/*
 * Common options for cp and mv
 */
#define CONFIG_FEATURE_PRESERVE_HARDLINKS 1

/*
 * Common options for ls and more
 */
#undef CONFIG_FEATURE_AUTOWIDTH

/*
 * Common options for df, du, ls
 */
#define CONFIG_FEATURE_HUMAN_READABLE 1

/*
 * Common options for md5sum, sha1sum
 */
#define CONFIG_FEATURE_MD5_SHA1_SUM_CHECK 1

/*
 * Console Utilities
 */
#undef CONFIG_CHVT
#define CONFIG_CLEAR 1
#undef CONFIG_DEALLOCVT
#undef CONFIG_DUMPKMAP
#undef CONFIG_LOADFONT
#undef CONFIG_LOADKMAP
#undef CONFIG_OPENVT
#define CONFIG_RESET 1
#undef CONFIG_SETKEYCODES

/*
 * Debian Utilities
 */
#define CONFIG_MKTEMP 1
#undef CONFIG_PIPE_PROGRESS
#undef CONFIG_READLINK
#undef CONFIG_RUN_PARTS
#undef CONFIG_START_STOP_DAEMON
#undef CONFIG_WHICH

/*
 * Editors
 */
#undef CONFIG_AWK
#define CONFIG_PATCH 1
#undef CONFIG_SED
#define CONFIG_VI 1
#define CONFIG_FEATURE_VI_COLON 1
#define CONFIG_FEATURE_VI_YANKMARK 1
#define CONFIG_FEATURE_VI_SEARCH 1
#define CONFIG_FEATURE_VI_USE_SIGNALS 1
#define CONFIG_FEATURE_VI_DOT_CMD 1
#define CONFIG_FEATURE_VI_READONLY 1
#define CONFIG_FEATURE_VI_SETOPTS 1
#define CONFIG_FEATURE_VI_SET 1
#define CONFIG_FEATURE_VI_WIN_RESIZE 1
#define CONFIG_FEATURE_VI_OPTIMIZE_CURSOR 1

/*
 * Finding Utilities
 */
#define CONFIG_FIND 1
#define CONFIG_FEATURE_FIND_MTIME 1
#define CONFIG_FEATURE_FIND_PERM 1
#define CONFIG_FEATURE_FIND_TYPE 1
#define CONFIG_FEATURE_FIND_XDEV 1
#define CONFIG_FEATURE_FIND_NEWER 1
#define CONFIG_FEATURE_FIND_INUM 1
#define CONFIG_GREP 1
#define CONFIG_FEATURE_GREP_EGREP_ALIAS 1
#define CONFIG_FEATURE_GREP_FGREP_ALIAS 1
#define CONFIG_FEATURE_GREP_CONTEXT 1
#undef CONFIG_XARGS

/*
 * Init Utilities
 */
#define CONFIG_INIT 1
#define CONFIG_FEATURE_USE_INITTAB 1
#define CONFIG_FEATURE_INITRD 1
#undef CONFIG_FEATURE_INIT_COREDUMPS
#undef CONFIG_FEATURE_EXTRA_QUIET
#define CONFIG_HALT 1
#undef CONFIG_POWEROFF
#undef CONFIG_REBOOT
#undef CONFIG_MESG

/*
 * Login/Password Management Utilities
 */
#undef CONFIG_USE_BB_PWD_GRP
#undef CONFIG_ADDGROUP
#undef CONFIG_DELGROUP
#undef CONFIG_ADDUSER
#undef CONFIG_DELUSER
#define CONFIG_GETTY 1
#undef CONFIG_FEATURE_U_W_TMP
#undef CONFIG_LOGIN
#undef CONFIG_PASSWD
#undef CONFIG_SU
#undef CONFIG_SULOGIN
#undef CONFIG_VLOCK

/*
 * Miscellaneous Utilities
 */
#undef CONFIG_ADJTIMEX
#define CONFIG_CROND 1
#undef CONFIG_FEATURE_CROND_CALL_SENDMAIL
#define CONFIG_CRONTAB 1
#define CONFIG_DC 1
#undef CONFIG_DEVFSD
#undef CONFIG_LAST
#undef CONFIG_HDPARM
#undef CONFIG_MAKEDEVS
#undef CONFIG_MT
#define CONFIG_RX 1
#define CONFIG_STRINGS 1
#define CONFIG_TIME 1
#undef CONFIG_WATCHDOGD

/*
 * Linux Module Utilities
 */
#undef CONFIG_INSMOD
#undef CONFIG_LSMOD
#undef CONFIG_MODPROBE
#undef CONFIG_RMMOD

/*
 * Networking Utilities
 */
#undef CONFIG_FEATURE_IPV6
#define CONFIG_ARPING 1
#define CONFIG_FTPGET 1
#define CONFIG_FTPPUT 1
#define CONFIG_HOSTNAME 1
#undef CONFIG_HTTPD
#define CONFIG_IFCONFIG 1
#define CONFIG_FEATURE_IFCONFIG_STATUS 1
#define CONFIG_FEATURE_IFCONFIG_SLIP 1
#define CONFIG_FEATURE_IFCONFIG_MEMSTART_IOADDR_IRQ 1
#define CONFIG_FEATURE_IFCONFIG_HW 1
#define CONFIG_FEATURE_IFCONFIG_BROADCAST_PLUS 1
#define CONFIG_IFUPDOWN 1
#undef CONFIG_FEATURE_IFUPDOWN_IP
#undef CONFIG_FEATURE_IFUPDOWN_IP_BUILTIN
#undef CONFIG_FEATURE_IFUPDOWN_IPV4
#undef CONFIG_FEATURE_IFUPDOWN_IPV6
#undef CONFIG_FEATURE_IFUPDOWN_IPX
#undef CONFIG_FEATURE_IFUPDOWN_MAPPING
#undef CONFIG_INETD
#undef CONFIG_IP
#undef CONFIG_IPCALC
#undef CONFIG_IPADDR
#undef CONFIG_IPLINK
#undef CONFIG_IPROUTE
#undef CONFIG_IPTUNNEL
#undef CONFIG_NAMEIF
#define CONFIG_NC 1
#define CONFIG_NETSTAT 1
#undef CONFIG_NSLOOKUP
#define CONFIG_PING 1
#define CONFIG_FEATURE_FANCY_PING 1
#undef CONFIG_ROUTE
#define CONFIG_TELNET 1
#define CONFIG_FEATURE_TELNET_TTYPE 1
#undef CONFIG_FEATURE_TELNET_AUTOLOGIN
#undef CONFIG_TELNETD
#define CONFIG_TFTP 1
#define CONFIG_FEATURE_TFTP_GET 1
#define CONFIG_FEATURE_TFTP_PUT 1
#define CONFIG_FEATURE_TFTP_BLOCKSIZE 1
#undef CONFIG_FEATURE_TFTP_DEBUG
#define CONFIG_TRACEROUTE 1
#define CONFIG_FEATURE_TRACEROUTE_VERBOSE 1
#define CONFIG_VCONFIG 1
#define CONFIG_WGET 1
#define CONFIG_FEATURE_WGET_STATUSBAR 1
#define CONFIG_FEATURE_WGET_AUTHENTICATION 1
#undef CONFIG_FEATURE_WGET_IP6_LITERAL

/*
 * udhcp Server/Client
 */
#undef CONFIG_UDHCPD
#define CONFIG_UDHCPC 1
#undef CONFIG_FEATURE_UDHCP_SYSLOG
#undef CONFIG_FEATURE_UDHCP_DEBUG

/*
 * Process Utilities
 */
#define CONFIG_FREE 1
#define CONFIG_KILL 1
#define CONFIG_KILLALL 1
#undef CONFIG_PIDOF
#define CONFIG_PS 1
#undef CONFIG_RENICE
#define CONFIG_TOP 1
#define CONFIG_FEATURE_CPU_USAGE_PERCENTAGE 1
#define CONFIG_UPTIME 1
#define CONFIG_SYSCTL 1

/*
 * Another Bourne-like Shell
 */
#undef CONFIG_FEATURE_SH_IS_ASH
#undef CONFIG_FEATURE_SH_IS_HUSH
#undef CONFIG_FEATURE_SH_IS_LASH
#define CONFIG_FEATURE_SH_IS_MSH 1
#undef CONFIG_FEATURE_SH_IS_NONE
#undef CONFIG_ASH
#define CONFIG_HUSH 1
#undef CONFIG_LASH
#define CONFIG_MSH 1

/*
 * Bourne Shell Options
 */
#undef CONFIG_FEATURE_SH_EXTRA_QUIET
#define CONFIG_FEATURE_SH_STANDALONE_SHELL 1
#define CONFIG_FEATURE_COMMAND_EDITING 1
#define CONFIG_FEATURE_COMMAND_HISTORY 15
#undef CONFIG_FEATURE_COMMAND_TAB_COMPLETION
#define CONFIG_FEATURE_SH_FANCY_PROMPT 1

/*
 * System Logging Utilities
 */
#undef CONFIG_SYSLOGD
#undef CONFIG_LOGGER

/*
 * Linux System Utilities
 */
#define CONFIG_DMESG 1
#define CONFIG_FBSET 1
#define CONFIG_FEATURE_FBSET_FANCY 1
#define CONFIG_FEATURE_FBSET_READMODE 1
#undef CONFIG_FDFLUSH
#undef CONFIG_FDFORMAT
#undef CONFIG_FDISK
#undef CONFIG_FREERAMDISK
#undef CONFIG_FSCK_MINIX
#undef CONFIG_MKFS_MINIX
#define CONFIG_GETOPT 1
#define CONFIG_HEXDUMP 1
#undef CONFIG_HWCLOCK
#undef CONFIG_LOSETUP
#undef CONFIG_MKSWAP
#define CONFIG_MORE 1
#define CONFIG_FEATURE_USE_TERMIOS 1
#undef CONFIG_PIVOT_ROOT
#undef CONFIG_RDATE
#undef CONFIG_SWAPONOFF
#define CONFIG_MOUNT 1
#define CONFIG_NFSMOUNT 1
#define CONFIG_UMOUNT 1
#define CONFIG_FEATURE_MOUNT_FORCE 1

/*
 * Common options for mount/umount
 */
#define CONFIG_FEATURE_MOUNT_LOOP 1
#undef CONFIG_FEATURE_MTAB_SUPPORT

/*
 * Debugging Options
 */
#undef CONFIG_DEBUG
